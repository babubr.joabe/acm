#!/bin/bash
# ACM - Automatic Code Minification
# Autor: Joabe Braga <babubr.joabe@gmail.com>

ACM_CONFIG_PATH="$HOME/.acm"
ACM_CONFIG_FILE="$HOME/.acm/config"
DEFAULT_ENCODING="";
EXT=""
EXT_MIN=""
YUI_PATH=""
LOAD_STATUS=0

# Obtem o caminho para o arquivo executável do Yui Compressor e armazena-o na 
# variável YUI_PATH.
function configurarPathYui(){
    echo "Digite o campinho completo para o arquivo Yui Compressor:"
    read YUI_PATH

    # Verifica se o arquivo .jar do Yui Compressor existe
    if [ -f $YUI_PATH ]; then
        # Torna o arquivo executável
        sudo chmod +x $YUI_PATH
    else
        echo "Arquivo não econtrado."
        configurarPathYui
    fi
}

# Obtem o encoding padrão para geração dos arquivos minificados e armazena-o na 
# variável DEFAULT_ENCODING.
configurarEncoding(){
    echo "Escolha o encoding padrão dos arquivos minificados. Digite I para ISO-8859-1 ou U para UTF-8. [I/U]"
    read ENCODING

    if [ $ENCODING != 'I' -a $ENCODING != 'U' ]; then
        echo "Opção inválida."
        configurarEncoding
    elif [ $ENCODING == 'U' ]; then
        DEFAULT_ENCODING="utf-8"
    else
        DEFAULT_ENCODING="iso-8859-1"
    fi
}

# Define os parâmetros essenciais para o funcionamento do script e salva-os num 
# arquivo texto no direto .acm localizado na home do usuário (/home/user/.acm).
function configurar(){
    configurarPathYui
    configurarEncoding

    # Verifica se o diretório de configuração do ACM já existe.
    if ! [ -d $ACM_CONFIG_PATH ]; then
        mkdir $ACM_CONFIG_PATH
    fi

    # Escreve no arquivo de configuração o caminho para o arquivo .jar e encoding padrão.
    echo "$YUI_PATH:$DEFAULT_ENCODING" > $ACM_CONFIG_FILE

    # Se o executável já existir, será apagado.
    if [ -f /usr/bin/acm ]; then
        sudo rm /usr/bin/acm
    fi

    # Copia o script para PATH.
    sudo cp "$0" /usr/bin/acm && sudo chmod +x /usr/bin/acm

    # Se o executável já existir, será apagado.
    if [ -f /usr/bin/acm ]; then
        echo "ACM configurado com sucesso!"
    else
        echo "Não foi possível mover o script para o diretório de executáveis."
    fi

    sleep 1s
}

# Lê o arquivo de configuração e hidrata as variáveis YUI_PATH DEFAULT_ENCODING.
function loadConfig(){
    if ! [ -f $ACM_CONFIG_FILE ]; then
        echo "Você ainda não configurou o ACM. Execute o seguinte comando para configurar: sudo ./acm.sh config"
    else
        STR_CONFIG=`cat $ACM_CONFIG_FILE`

        # Explode a string utilizando como referẽncia o ":"
        ARR_CONFIG=(${STR_CONFIG//:/ })

        # Extrai o caminho para o arquivo .jar
        YUI_PATH=${ARR_CONFIG[0]}

        # Extrai o enconding padrão.
        DEFAULT_ENCODING=${ARR_CONFIG[1]}

        LOAD_STATUS=1
    fi
}

# Verifica se o usuário passou como primeiro argumento  string "config.". Se sim, 
# o processo de configuração do ACM será iniciado.
if [ "$1" = "config" ]; then
    configurar
    exit
elif [ "$1" = "js" -o "$1" = "css" ]; then
    EXT=$1
    EXT_MIN=".min.$1"

# Verifica se o argumento $2 contem os caracteres .js/.css no final da string. 
# Se a condição for verdadeira, siginifa que o usuário informou o nome de um 
# arquivo javascript ou CSS para ser compactado.
#
# O -i indicica que a busca irá igonorar maiúculas/minúsculas.
# O -c retorna um contagrem ao invés da linha encontrada pelo grep, o que 
# possibilita checar se o resultado é diferente de zero.
elif [ $(echo $1 | grep -ic '[.]js$') -ne 0 ]; then
    FILES_GIT=`git status --porcelain *$1`
    EXT='js'
    EXT_MIN='.min.js'
elif [ $(echo $1 | grep -ic '[.]css$') -ne 0 ]; then
    FILES_GIT=`git status --porcelain *$1`
    EXT='css'
    EXT_MIN='.min.css'
else
    echo "O argumento 1 é inválido. Informe uma das opções seguintes: [js/css/filename]."
    exit
fi

# Carrega as configurações do ACM.
loadConfig

# Verifica se as configurações foram carregadas.
if [ $LOAD_STATUS -eq 0 ]; then
    echo "Não foi possível carregar as configurações do ACM."
    exit
fi

# Abre o diretório no qual o ACM foi chamado. Espera-se que esse diretório seja 
# um repositório GIT.
cd `pwd -P`

if [ "$FILES_GIT" = '' ]; then

    # Busca por arquivos modificados e não versionados.
    if [ "$2" = "all" ]; then
        echo "Buscando por arquivos novos/modificados..."
        sleep 1s

        # Busca apenas por arquivos modificados e também novos arquivos (untracked
        # files) com a extensão ".js".
        FILES_GIT=`git status --porcelain | grep -i [.]$EXT$ | grep -i -v .min.$EXT$`

    # Apenas arquivos modificados.
    elif [ "$2" = "mod" ]; then
        echo "Buscando por arquivos modificados..."
        sleep 1s

        # Busca apenas pelos arquivos modificados com a extensão ".js".
        FILES_GIT=`git status --porcelain | grep -i [.]$EXT$ | grep -i -v .min.$EXT$ | grep "M "`

    fi
fi

if [ "$FILES_GIT" = '' ]; then
    echo "Nenhum arquivo para ser minificado."
    exit
else
    echo "Processando arquivos..."
fi

# Verifica se o segundo argumento foi informado. Esse argumento indica o 
# encoding do arquivo minificado que será gerado. Ele será utilizado quando o 
# usuário desejar minificar com um encoding diferente do que foi definido na 
# configuração inicial do ACM.
if [ "$2" = "iso" -o "$3" = "iso" ]; then
    DEFAULT_ENCODING="iso-8859-1"
elif [ "$2" = "utf8" -o "$3" = "utf8" ]; then
    DEFAULT_ENCODING="utf-8"
fi

# Explode a string armazenada na variável FILES_JS e percorre o array 
# minificando os arquivos. Cada elemento desse array é PATH de um aqruivo a ser 
# minificado.
while IFS=' ' read -ra ADDR; do
    for i in "${ADDR[@]}"; do
        
        # Verifica se $i é um arquivo.
        if [ -f $i ]; then

            # Remove a extensão ".js/.css" e adiciona a extensão ".min.js/.min.css".
            if [ "$EXT" = "js" ]; then 
                FILE_MIN=${i:: -3}$EXT_MIN
            elif [ "$EXT" = "css" ]; then
                FILE_MIN=${i:: -4}$EXT_MIN
            else
                echo "A extensão $EXT do arquivo $i é inválida"
                exit
            fi

            # Chama programa Yui Compressor que fará a minificação.
            java -jar "$YUI_PATH" "./$i" -o "./$FILE_MIN" --charset "$DEFAULT_ENCODING"
        fi
    done
done <<< "$FILES_GIT"

echo "Yui Compressor executado com sucesso!"