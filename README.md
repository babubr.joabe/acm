Usage: acm [options] [context] [encoding]

ACM é um simples script que tem como objetivo automatizar a minificação de
arquivos com o Yui Compressor. O ACM pega aquivos (JavaScript/CSS) de um
repositório GIT (utilizando o comando "git status") e processa-os, um a um, 
gerando assim suas variantes minificadas.

Options:
    config,   Inicia a configuração do ACM. Este argumento deve ser informado na primeira execução do script.
    js,       apenas arquivos JavaScript serão minificados
    css,      apenas arquivos CSS serão minificados
    filename, nome de um arquivo específico a ser minificado. Esse argumento pode
              ter apenas um (1) nome de arquivo.

Context:

Este contexto é em relação ao GIT. Este argumento pode ser suprimido quando o 
primeiro argumento for filename.

    mod,      filtra arquivos versionados que estão modificados arguardando commit.
    all,      filtra arquivos modificados e não versionados (untracked files).

Encoding:

Este argumento pode ser suprimido sempre. Nesse caso o ACM irá utilizar o encoding
padrão definido na configuração inicial do script.

    utf8,     Define UTF-8 como conjunto de caracteres para codificação do arquivo de saída.
    iso,      Define ISO-8859-1 como conjunto de caracteres para codificação do arquivo de saída.


Exemplos:

    ./acm.sh config,    Primeira execução do script.
    
    acm js mod,         Minifica todos os arquivos "js" modificados.
    acm js mod utf8     Minifica todos os arquivos "js" modificados utilizando o conjunto de caracteres UTF-8.
    acm js all          Minifica todos os arquivos "js", tanto os modificados quanto os não versionados.
    acm index.css       Minifica o arquivo index.css